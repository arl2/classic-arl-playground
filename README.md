# Classic ARL

## Preface 

This repository is meant to be an introduction to using palaestrAI.
Please treat it as **read-only** unless you want to improve its' original purpose.
If you want to use it as base for your own experiment, push your changes to a different origin (,e.g., in gitlab.com/arl-experiments).

## Introduction

Classic ARL -- where it all began. 
The original ARL scenario consisted of two reinforcement learning agents with contradicting goals.
The agents were acting on a power grid environment with almost full control. 
The environment itself used the technical connection rules (German: Technische Anschlussregeln, TAR) to keep itself in a healthy state.
One of the agents had the goal to assist the TAR and keep the grid in a healthy state while the other had to create some kind of attack.
Each of the agents had one indivual training phase and one joint training phase, followed by a joined test phase.

This repository aims to reproduce this scenario using the [palaestrAI](https://docs.palaestr.ai) framework together with [Midas](https://midas-mosaik.gitlab.io/midas) as power grid environment.

## Preparation

There is actually no coding required to run this scenario.
But you still need to set up a virtual environment using a version of Python 3.10 and install a few packages, specified in a requirements file.
If you already know how to proceed from here, you can skip the rest of this section.

If you're coming from PyCharm/conda/some other FotM python packaging tool **and still don't know how to proceed**, then stop using those and follow this guide.
First, if you're a Windows user, you have to use WSL[todo: Add Link] since palaestrAI will not work on bare Windows [todo: Link to issue].
If you use OS X, I hope you either already know how to install the required things or, at least, that you're able to translate the steps to your system.
Feel free to send me any hints or changes that are required for OS X.

The next logical step is to make sure you have the correct Python version installed:

```bash
which python3
python3.10 --version
``` 

The first command should give you `/usr/bin/python3` and the second should give something like `Python 3.10.13`.
If the second command fails, you have to install Python 3.10 on your system.
**Arch Linux** users can simply get it from the AUR:

```bash
git clone https://aur.archlinux.org/python310.git && cd python310
makepkg -si
```

For Debian-based distros like **Ubuntu** you have to add a new apt repository first

```bash
sudo apt update && sudo apt upgrade -y
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update && sudo apt install python3.10
# sudo apt install python3.10-venv
```

The above commands to check the Python installation should work now.

Now, let's assume you have created a directory called `MyClassicARL` in your home directory.
Let's switch into that directory, create the virtual environment there, and activate it:

```bash
cd ~/MyClassicARL
python3.10 -m venv carl-venv
source ./carl-venv/bin/activate
```

The next step is to clone this repository (install git first if it is not installed on your system -- check with `git --version`)

```
git clone https://gitlab.com/arl-experiments/classic-arl-playground.git
```

The last step is to finally install the additional requirements with pip

```bash
pip install -r requirements.txt
```

If there where no errors during installation, you can test the palaestrai command line tool:

```bash
palaestrai --version
```

which should give you `palaestrai, version 3.5.1` (by the time of writing this guide, the actual version might be higher).


## Configuring the Database

PalaestrAI stores all experiment results in a SQL database. 
By default, this is a sqlite database file but a postgresql database is possible as well.
The latter is faster and has more features, but requires a few more configuration steps.
The database-to-use has to be defined in the runtime configuration file of palaestrAI.

If you don't have a palaestrAI runtime configuration file yet, you can create a system-wide file with the following commands:

```bash 
mkdir -p ~/.config/palaestrai
palaestrai runtime-config-show-default > ~/.config/palaestrai/runtime-conf.yaml
```

If you want to use the slower sqlite database, you now can create the database with

```bash
palaestrai database-create
```

and skip the next subsection (but don't miss the configuration of Midas).

### Configuring a postgresql database

The easiest way is to set up postgresql as Docker container.
This means, you have to make sure Docker is installed on your system.

```bash
docker --version
```

There might be two things going wrong with this command.
First, you could get a message like `command not found`, which means you have to install Docker first.
You may find it in your distribution's package manager, e.g. for Arch Linux:

```bash
sudo pacman -S docker
```

Debian-based distros have to to a [few more steps](https://kinsta.com/de/blog/installieren-docker-ubuntu/)

```bash
sudo apt update && sudo apt install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io
```

Next, you need to enable the Docker system service

```bash
sudo systemctl enable --now docker.service
```


Now the Docker command should be found by the system.
However, the second possible error has to do with permissions.
By default, you need to be superuser to use Docker.
This is a safety mechanism and if you want to keep it, you have to start each Docker command with sudo instead:

```bash
sudo docker --version
```

Alternatively, you can add your user to the Docker group

```bash
sudo usermod -aG docker $USER
```

You might need to log out and log in again to be able to use Docker commands without sudo.
But now any command should work without errors.

Finally, we can create the Docker container with the database

```bash
docker run -d --name palaestraidb -p 127.0.0.1:5434:5432 -e POSTGRES_PASSWORD=password timescale/timescaledb:latest-pg16
```

TODO: specify command to use local storage instead of container-internal storage

You only have to run this command once. 
After a system reboot, you can start the container with

```bash
docker start palaestraidb
``` 

The second-last step is to open your runtime configuration file at `~/.config/runtime-conf.yaml` with your favorite text editor and change the line starting with `store_uri:` to 

```yaml
store_uri: postgresql://postgres:password@127.0.0.1:5434/palaestrai
```

Here, `password` is the same string that you entered in the docker run command before

Finally, you can execute the palaestrai database command:

```bash
palaestrai database-create
```

To test that command, you can log into the container with

```bash
docker exec -it palaestraidb psql -U postgres
```

which throws you into the postgres shell, where you can check the existence of the database with

```
\l
```

The resulting table should contain an entry called `palaestrai`.

### Configuring Midas

To save some time during the simulation, it is recommended (but not necessary) to configure Midas beforehand.
There are two commands to perform configuration and downloading of the datasets:

```bash
midasctl configure -a
midasctl download
```

Should take a minute or two, but that's all.
You're ready to take off.

## Running the Experiment

If you set up everything correctly, running the experiment is actually really simple.
First, it is time to jump into the classic ARL directory:

```bash
cd classic-arl-playground
``` 

Then type two more commands:

```bash
arsenai generate palaestrai-experiment-files/carl_reference_experiment.yml
palaestrai start palaestrai-runfiles/Classic-ARL-Experiment_run-0.yml
```

This will start the *reference* experiment, with three (todo: include all six) phases and two agents with contradicting goals.

## Analysis

If everything worked well, all experiment results were written into the database.
The most simply analysis you can perform on the data is exemplarily shown in the analysis notebook `analysis/carl_reference_analysis.ipynb`
You can start the notebook by typing:

```bash
jupyter notebook
```

The details of the analysis is described in the notebook itself, so please continue reading there.

