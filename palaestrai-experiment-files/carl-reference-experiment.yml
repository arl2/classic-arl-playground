%YAML 1.2
# The Classic ARL reference experiment

---
uid: "Classic-ARL-Experiment"  # Base name for the whole experiment
seed: 2022  # Seed used to initialize the random number generator
version: 3.5.1  # Target palaestrAI version
output: palaestrai-runfiles  # Directory where the run files will be saved
repetitions: 1  # How often each run will be repeated. Each repetition is a separate file
max_runs: 100  # Maximum number of runs (excluding repetitions) created by the DoE generator
definitions:  # In this section all the components will be defined
  ###########################################
  # The environment' section
  environments:
    midasmv_tar_ms:  
      environment:
        name: palaestrai_mosaik:MosaikEnvironment
        uid: midas_powergrid
        params: 
          module: midas.tools.palaestrai:Descriptor  # Always the same if you use MIDAS
          description_func: describe  # Always the same if you use MIDAS
          instance_func: get_world  # Always the same if you use MIDAS
          arl_sync_freq: &step_size 60 # &step_size 1 means to set variable step_size=1
          end: &end 86460  # One step extra because one step gets "lost"
          silence_missing_input_connections_warning: True

          params:  # Parameters that are passed to description_func and
                    # instance_func, Part that is being sent to MIDAS directly
            name: carl_cigre_base # scenario name
            config: midas-scenarios/classic-arl.yml # relative to current working directory; the file that is called .config/midas/ClasicARL.yml on my laptop
            end: *end
            step_size: *step_size # &step_size put here from above with value 1
            start_date: 2020-04-13 12:00:00+0100 # Use ISO datestring or magic keywork 'random'
            mosaik_params: {addr: [127.0.0.1, 5678]} # You can change the port (5678) to something else (e.g. 5679) when 'address already in use'
            store_params: #everything in here overwrites what is written in classic-arl.yml in /midas-scenarios/
              buffer_size: 1000 # Every x steps, intermediate results are dumped to the midas store
              keep_old_files: true
              filename: carl_with_arl_base.hdf5
      reward:
        name: midas.tools.palaestrai.rewards:ExtendedGridHealthReward
  ###########################################
  # The agents' section
  agents:
    gandalf_single:
      name: Gandalf Single
      brain: &sac_brain
        name: harl.sac.brain:SACBrain
        params:
          fc_dims: [48, 48]
          update_after: 1000
          batch_size: 500
          update_every: 200
      muscle: &sac_muscle
        name: harl.sac.muscle:SACMuscle
        params: {}
      objective: &defender_objective
        name: midas.tools.palaestrai:ArlDefenderObjective
        params: {} 
    gandalf_ac:
      name: Gandalf Autocurriculum
      brain: *sac_brain
      muscle: *sac_muscle
      objective: *defender_objective
    sauron_single:
      name: Sauron Single
      brain: *sac_brain
      muscle: *sac_muscle
      objective: &attacker_objective
        name: midas.tools.palaestrai:ArlAttackerObjective
        params: {}
    sauron_ac:
      name: Sauron Autocurriculum
      brain: *sac_brain
      muscle: *sac_muscle
      objective: *attacker_objective
  ###########################################
  # The sensors' section
  sensors:
    all_sensors: 
      midas_powergrid:
        - midas_powergrid.Powergrid-0.0-bus-1.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-1.va_degree
        - midas_powergrid.Powergrid-0.0-bus-10.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-10.va_degree
        - midas_powergrid.Powergrid-0.0-bus-11.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-11.va_degree
        - midas_powergrid.Powergrid-0.0-bus-12.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-12.va_degree
        - midas_powergrid.Powergrid-0.0-bus-13.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-13.va_degree
        - midas_powergrid.Powergrid-0.0-bus-14.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-14.va_degree
        - midas_powergrid.Powergrid-0.0-bus-2.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-2.va_degree
        - midas_powergrid.Powergrid-0.0-bus-3.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-3.va_degree
        - midas_powergrid.Powergrid-0.0-bus-4.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-4.va_degree
        - midas_powergrid.Powergrid-0.0-bus-5.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-5.va_degree
        - midas_powergrid.Powergrid-0.0-bus-6.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-6.va_degree
        - midas_powergrid.Powergrid-0.0-bus-7.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-7.va_degree
        - midas_powergrid.Powergrid-0.0-bus-8.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-8.va_degree
        - midas_powergrid.Powergrid-0.0-bus-9.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-9.va_degree
        - midas_powergrid.Powergrid-0.0-line-0.loading_percent
        - midas_powergrid.Powergrid-0.0-line-0.in_service
        - midas_powergrid.Powergrid-0.0-line-1.loading_percent
        - midas_powergrid.Powergrid-0.0-line-1.in_service
        - midas_powergrid.Powergrid-0.0-line-10.loading_percent
        - midas_powergrid.Powergrid-0.0-line-10.in_service
        - midas_powergrid.Powergrid-0.0-line-11.loading_percent
        - midas_powergrid.Powergrid-0.0-line-11.in_service
        - midas_powergrid.Powergrid-0.0-line-12.loading_percent
        - midas_powergrid.Powergrid-0.0-line-12.in_service
        - midas_powergrid.Powergrid-0.0-line-13.loading_percent
        - midas_powergrid.Powergrid-0.0-line-13.in_service
        - midas_powergrid.Powergrid-0.0-line-14.loading_percent
        - midas_powergrid.Powergrid-0.0-line-14.in_service
        - midas_powergrid.Powergrid-0.0-line-2.loading_percent
        - midas_powergrid.Powergrid-0.0-line-2.in_service
        - midas_powergrid.Powergrid-0.0-line-3.loading_percent
        - midas_powergrid.Powergrid-0.0-line-3.in_service
        - midas_powergrid.Powergrid-0.0-line-4.loading_percent
        - midas_powergrid.Powergrid-0.0-line-4.in_service
        - midas_powergrid.Powergrid-0.0-line-5.loading_percent
        - midas_powergrid.Powergrid-0.0-line-5.in_service
        - midas_powergrid.Powergrid-0.0-line-6.loading_percent
        - midas_powergrid.Powergrid-0.0-line-6.in_service
        - midas_powergrid.Powergrid-0.0-line-7.loading_percent
        - midas_powergrid.Powergrid-0.0-line-7.in_service
        - midas_powergrid.Powergrid-0.0-line-8.loading_percent
        - midas_powergrid.Powergrid-0.0-line-8.in_service
        - midas_powergrid.Powergrid-0.0-line-9.loading_percent
        - midas_powergrid.Powergrid-0.0-line-9.in_service
    ###########################################
  # The actuators' section
  actuators:
    attacker_actuators:
      midas_powergrid:
        - midas_powergrid.Powergrid-0.0-load-4-6.p_mw
        - midas_powergrid.Powergrid-0.0-load-4-6.q_mvar
        - midas_powergrid.Powergrid-0.0-load-5-7.p_mw
        - midas_powergrid.Powergrid-0.0-load-5-7.q_mvar
        - midas_powergrid.Powergrid-0.0-load-6-8.p_mw
        - midas_powergrid.Powergrid-0.0-load-6-8.q_mvar
        - midas_powergrid.Powergrid-0.0-load-7-9.p_mw
        - midas_powergrid.Powergrid-0.0-load-7-9.q_mvar
        - midas_powergrid.Powergrid-0.0-load-8-10.p_mw
        - midas_powergrid.Powergrid-0.0-load-8-10.q_mvar
        - midas_powergrid.Powergrid-0.0-load-9-11.p_mw
        - midas_powergrid.Powergrid-0.0-load-9-11.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-4-6.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-4-6.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-5-7.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-5-7.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-6-8.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-6-8.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-7-9.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-7-9.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-8-10.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-8-10.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-9-11.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-9-11.q_mvar
    defender_actuators:
      midas_powergrid:
        - midas_powergrid.Powergrid-0.0-load-0-1.p_mw
        - midas_powergrid.Powergrid-0.0-load-0-1.q_mvar
        - midas_powergrid.Powergrid-0.0-load-1-3.p_mw
        - midas_powergrid.Powergrid-0.0-load-1-3.q_mvar
        - midas_powergrid.Powergrid-0.0-load-10-12.p_mw
        - midas_powergrid.Powergrid-0.0-load-10-12.q_mvar
        - midas_powergrid.Powergrid-0.0-load-11-13.p_mw
        - midas_powergrid.Powergrid-0.0-load-11-13.q_mvar
        - midas_powergrid.Powergrid-0.0-load-12-14.p_mw
        - midas_powergrid.Powergrid-0.0-load-12-14.q_mvar
        - midas_powergrid.Powergrid-0.0-load-2-4.p_mw
        - midas_powergrid.Powergrid-0.0-load-2-4.q_mvar
        - midas_powergrid.Powergrid-0.0-load-3-5.p_mw
        - midas_powergrid.Powergrid-0.0-load-3-5.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-0-1.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-0-1.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-1-3.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-1-3.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-10-12.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-10-12.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-11-13.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-11-13.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-12-14.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-12-14.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-2-4.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-2-4.q_mvar
        - midas_powergrid.Powergrid-0.0-sgen-3-5.p_mw
        - midas_powergrid.Powergrid-0.0-sgen-3-5.q_mvar
  ###########################################
  # The simulation section
  simulation:
    taking_turns: # User-defined name of one simulation controller.
      name: palaestrai.simulation:TakingTurns
      conditions:
        - name: palaestrai.simulation:VanillaSimControllerTerminationCondition
          params: {}
    vanilla_sim: # User-defined name of one simulation controller.
      name: palaestrai.simulation:Vanilla
      conditions:
        - name: palaestrai.simulation:VanillaSimControllerTerminationCondition
          params: {}
  ###########################################
  # The phase-configs' section
  phase_config:
    training:
      mode: train
      worker: 1
      episodes: 1
    test:
      mode: test
      worker: 1
      episodes: 1
  ###########################################
  # The run-configs' section
  run_config:
    condition:
      name: palaestrai.experiment:VanillaRunGovernorTerminationCondition
      params: {}

schedule: 
  - phase_attacker_single:
      environments: [[midasmv_tar_ms]]  # one factor, one level
      agents: [[sauron_single]] # one factor, one levels
      simulation: [taking_turns] # one factor, one level
      phase_config: [training] # one factor, one level
      sensors:
        sauron_single: [all_sensors]
        gandalf_single: [all_sensors]
      actuators:
        sauron_single: [attacker_actuators]  
        gandalf_single: [defender_actuators]  
  - phase_defender_single:
      agents: [[gandalf_single]]
  - phase_autocurriculum:
      agents: [[sauron_ac, gandalf_ac]]
      sensors:
        sauron_ac: [all_sensors]
        gandalf_ac: [all_sensors]
      actuators:
        sauron_ac: [attacker_actuators]
        gandalf_ac: [defender_actuators]